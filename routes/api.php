<?php

use Illuminate\Http\Request;

Route::group(['prefix' => 'v1' , 'namespace' => 'Api\v1'] , function (){
    Route::get('articles', 'ArticleController@articles');
    Route::post('comment', 'ArticleController@comment');
    Route::post('login', 'UserController@login');
    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });
});
