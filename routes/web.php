<?php

// Home Routes
Route::get('/','HomeController@index');
Route::get('/search','HomeController@search');
//Route::get('/articles', 'ArticleController@index');
Route::get('/courses', 'CourseController@index');
Route::get('/articles/{articleSlug}', 'ArticleController@single');
Route::get('/courses/{courseSlug}', 'CourseController@single')->middleware('auth');
Route::post('/comment', 'HomeController@comment')->middleware('auth:web');
Route::get('/user/active/email/{token}' , 'UserController@activation')->name('activation.account');


// Sitemap
Route::get('/sitemap','SitemapController@index');
Route::get('/sitemap-articles','SitemapController@articles');

// Feeds
Route::get('/feed/articles','FeedController@articles');

// Telegram Bot
Route::get('/telegram','TelegramController@telegram');

// Payment
Route::group(['middleware'=>'auth:web'],function (){
    Route::post('/course/payment','CourseController@payment');
    Route::get('/course/payment/checker','CourseController@checker');


    Route::group(['prefix' => '/user/panel'] , function(){
        Route::get('/' , 'UserController@index')->name('user.panel');
        Route::get('/history' , 'UserController@history')->name('user.panel.history');
        Route::get('/vip' , 'UserController@vip')->name('user.panel.vip');

        Route::post('/payment' , 'UserController@vipPayment')->name('user.panel.vip.payment');
        Route::get('/checker' , 'UserController@vipChecker')->name('user.panel.vip.checker');
    });
});
//  User Verification
Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');
// Panel Routs
Route::group(['namespace'=>'Admin','prefix'=>'admin', 'middleware' => ['auth:web' , 'checkAdmin']],function (){

    Route::get('/' , 'PanelController@index')->name('panel');
    Route::post('/panel/upload-image' , 'PanelController@uploadImageSubject');
    Route::resource('articles' , 'ArticleController');
    Route::resource('courses' , 'CourseController');
    Route::resource('episodes' , 'EpisodeController');

    // Comment Section
    Route::get('comments/unapproved' , 'CommentController@unapproved');
    Route::resource('comments' , 'CommentController');

    // Payment Section
    Route::get('payments/unsuccessful' , 'PaymentController@unsuccessful');
    Route::resource('payments' , 'PaymentController');

    Route::resource('roles' , 'RoleController');
    Route::resource('permissions' , 'PermissionController');

    Route::group(['prefix' => 'users'],function (){
        Route::get('/' , 'UserController@index');
        Route::resource('level' , 'LevelManageController' , ['parameters' => ['level' => 'user']]);
        Route::delete('/{user}/destroy' , 'UserController@destroy')->name('users.destroy');
    });
});
// Authentication Routes
Route::group(['namespace' => 'Auth'] , function (){
    // Authentication Routes...
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
    Route::get('logout', 'LoginController@logout')->name('logout');

    // Registration Routes...
    Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'RegisterController@register');

    // Password Reset Routes...
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'ResetPasswordController@reset');

    // Login And Register with google Account
    Route::get('login/google', 'LoginController@redirectToProvider');
    Route::get('login/google/callback', 'LoginController@handleProviderCallback');
});

Route::get('/home', 'HomeController@index')->name('home');



