<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable , HasRole,HasApiTokens;

    protected $fillable = [
        'name', 'email', 'password','verified','viptime'
    ];

    protected $hidden = [
        'password', 'remember_token','api_token'
    ];
    public function verifyUser()
    {
        return $this->hasOne(VerifyUser::class);
    }
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function article()
    {
        return $this->hasMany(Article::class);
    }
    public function course()
    {
        return $this->hasMany(Course::class);
    }

    public function isAdmin()
    {
        return $this->level == 'admin' ? true : false;
    }
    public function payments()
    {
        return $this->hasMany(Payment::class);
    }
    public function checkLearning($course)
    {
        return !! Learning::where('user_id',$this->id)->where('course_id',$course)->first();
    }
    public function isActive()
    {
        return $this->viptime > Carbon::now() ? true : false;
    }
}
