<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleProviderCallback()
    {
        $socialite_user = Socialite::driver('google')->stateless()->user();
        $user = User::whereEmail($socialite_user->getEmail())->first();
        if (!$user) {
            $user = User::create([
                'name' => $socialite_user->getName(),
                'email' => $socialite_user->getEmail(),
                'password' => bcrypt($socialite_user->getId())
            ]);
        }


        if ($user->verified == 0) {
            $user->update([
                'verified' => 1
            ]);
        }
        auth()->loginUsingId($user->id);
        return redirect('/');
    }

    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
            'g-recaptcha-response' => 'required|recaptcha'
        ]);
    }
// OAuth With Passport
    public function loginWithRoocket()
    {
        $query = http_build_query([
            'client_id' => 3,
            'redirect_uri' => "http://localhost:7000/callback",
            'response_type' => 'code',
            'scope' => '',
        ]);
        return redirect("http://localhost:8000/oauth/authorize?" . $query);
    }

    public function loginCallback(Request $request)
    {
        $http = new Client();

        $response = $http->post('http://localhost:8000/oauth/token', [
            'form_params' => [
                'grant_type' => 'authorization_code',
                'client_id' => '3',
                'client_secret' => '7w3b9sjllUD2U9BbCZQj5G7NWgeyVBVN3oYLGgsj	',
                'redirect_uri' => 'http://localhost:7000/callback',
                'code' => $request->code,
            ],
        ]);

        $access_token = json_decode((string)$response->getBody(), true)['access_token'];
        $response = $http->get('http://localhost:8000/api/v1/users', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer " . $access_token
            ]
        ]);
        return $response;

    }
}
