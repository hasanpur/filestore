<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class ArticleController extends Controller
{

    public function single(Article $article)
    {
        //        Redis::incr("view.{$article->id}.articles");
        $article->increment('viewCount');
        $comments = $article->comments()
            ->where('approved', 1)
            ->where('parent_id', 0)
            ->with('comments')
            ->latest()->get();
        return view('Home.article', compact('article', 'comments'));
    }
}
