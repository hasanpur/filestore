<?php

namespace App\Http\Controllers\Api\v1;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response(['data' => ['message' => $validator->errors()->all(), 'status' => 400]], 400);
        }

        if (!auth()->validate(['email' => $request->email, 'password' => $request->password])) {
            return response(['data' => ['message' => 'unauthorised', 'status' => 401]], 401);
        }
        $user=User::whereEmail($request->input('email'))->first();
        $token=$user->createToken('Api Login Code')->accessToken();
        return response(['data' => ['message' => $token, 'status' => 200]], 200);

    }
}
