<?php

namespace App\Http\Controllers\Api\v1;

use App\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ArticleController extends Controller
{
    public function articles()
    {
        $article = Article::select('title', 'body', 'user_id')->get();
        return response(['data' => ['article' => $article, 'status' => 200]], 200);
    }

    public function comment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'body' => 'required',
        ]);
        if ($validator->fails()) {
            return response(['data' => ['article' => $validator->errors()->all(), 'status' => 403]], 403);
        }

    }

}
