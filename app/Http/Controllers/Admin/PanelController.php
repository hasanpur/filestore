<?php

namespace App\Http\Controllers\Admin;

use App\Payment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Morilog\Jalali\Jalalian;

class PanelController extends Controller
{
    public function index()
    {
        $month = 12;
        $paymentSuccess = Payment::spanningPayment($month, true);
        $paymentUnSuccess = Payment::spanningPayment($month, false);
        $labels = $this->getMonthName($month);
        $values['success']=$this->checkCount($paymentSuccess->pluck('published'),$month);
        $values['unsuccess']=$this->checkCount($paymentUnSuccess->pluck('published'),$month);
        return view('Admin.panel',compact(['labels','values']));
    }

    public function uploadImageSubject()
    {
        $this->validate(request(), [
            'upload' => 'required|mimes:jpeg,png,bmp',
        ]);

        $year = Carbon::now()->year;
        $imagePath = "/upload/images/{$year}/";

        $file = request()->file('upload');
        $filename = $file->getClientOriginalName();

        if (file_exists(public_path($imagePath) . $filename)) {
            $filename = Carbon::now()->timestamp . $filename;
        }

        $file->move(public_path($imagePath), $filename);
        $url = $imagePath . $filename;

        return "<script>window.parent.CKEDITOR.tools.callFunction(1 , '{$url}' , '')</script>";
    }

    private function getMonthName($month)
    {
        for ($i = 0;$i < $month;$i++){
            $names[] = jdate(Carbon::now()->subMonths($i))->format('%B');
    }
        return array_reverse($names);
    }

    private function checkCount($count,$month)
    {
        for ($i = 0;$i < $month;$i++){
            $new[] = empty($count[$i]) ? 0 : $count[$i];
        }
        return array_reverse($new);
    }
}

