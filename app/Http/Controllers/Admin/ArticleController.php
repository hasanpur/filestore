<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Http\Requests\ArticleRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticleController extends AdminController
{

    public function index()
    {
        $articles = Article::latest()->paginate(20);
        return view('Admin.articles.all' , compact('articles'));
    }

    public function create()
    {
        return view('Admin.articles.create');
    }


    public function store(ArticleRequest $request)
    {
        $imagesUrl = $this->uploadImages($request->file('images'));
        auth()->user()->article()->create(array_merge($request->all() , [ 'images' => $imagesUrl]));

        return redirect(route('articles.index'));
    }

    public function show(Article $article)
    {
        //
    }

    public function edit(Article $article)
    {
        return view('Admin.articles.edit' , compact('article'));
    }


    public function update(ArticleRequest $request, Article $article)
    {
        $file = $request->file('images');
        $inputs = $request->all();

        if($file) {
            $inputs['images'] = $this->uploadImages($request->file('images'));
        } else {
            $inputs['images'] = $article->images;
            $inputs['images']['thumb'] = $inputs['imagesThumb'];

        }

        unset($inputs['imagesThumb']);
        $article->update($inputs);

        return redirect(route('articles.index'));
    }

    public function destroy(Article $article)
    {
        $article->delete();
        return redirect(route('articles.index'));
    }
}

