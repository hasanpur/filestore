<?php

namespace App\Http\Controllers\Admin;

use App\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{

    public function index()
    {
        $comments = Comment::where('approved', 1)->latest()->paginate(20);
        return view('Admin.comments.all', compact('comments'));
    }


    public function unapproved()
    {
        $comments = Comment::where('approved', 0)->latest()->paginate(20);
        return view('Admin.comments.approved', compact('comments'));
    }

    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, Comment $comment)
    {
        $comment->update(['approved' => 1]);
        $comment->commentable->increment('commentCount');
        alert()->success('عملیات مورد نظر با موفقیت انجام شد', 'تایید شد');
        return back();
    }

    public function destroy(Comment $comment)
    {
        $comment->commentable->decrement('commentCount');
        $comment->delete();
        alert()->success('عملیات مورد نظر با موفقیت انجام شد', 'حذف شد');
        return back();
    }
}
