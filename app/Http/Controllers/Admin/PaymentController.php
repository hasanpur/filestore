<?php

namespace App\Http\Controllers\Admin;

use App\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentController extends Controller
{

    public function index()
    {
        $payments=Payment::with('user')->wherePayment(1)->latest()->paginate(20);
        return view('Admin.payments.all',compact('payments'));
    }


    public function unsuccessful()
    {
        $payments=Payment::with('user')->wherePayment(0)->latest()->paginate(20);
        return view('Admin.payments.unsuccessful',compact('payments'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Payment $payment)
    {
        //
    }


    public function edit(Payment $payment)
    {
        //
    }


    public function update(Request $request, Payment $payment)
    {
        $payment->update(['payment'=>1]);
        return back();
    }


    public function destroy(Payment $payment)
    {
        $payment->delete();
        return back();
    }
}
