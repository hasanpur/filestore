<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Course;
use App\Learning;
use App\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use SoapClient;

class CourseController extends Controller
{
    public $MerchantID ='46688bb0-9217-11e7-bbe1-005056a205be';

    public function index()
    {
        $courses= Course::filter()->paginate(12);
       return view('Home.all-courses',compact('courses'));
    }

    public function single(Course $course)
    {
        $course->increment('viewCount');
        // Redis
//        Redis::incr("view.{$course->id}.courses");
        $comments = $course->comments()->where('approved', 1)
            ->where('parent_id', 0)
            ->with('comments')
            ->latest()->get();
        return view('Home.courses', compact('course', 'comments'));
    }

    public function payment()
    {
        $this->validate(\request(), [
            'course_id' => 'required'
        ]);
        $course = Course::findOrFail(\request('course_id'));
        if (auth()->user()->checkLearning($course)){
            alert()->error('شما قبلا در این دوره ثبت نام کرده اید','دقت کنید')->persistent('خیلی خوب');
            return back();
        }

        if ($course->price == 0 && $course->type == 'vip') {
            alert()->error('این دوره قابل خریداری توسط شما نیست','دقت کنید')->persistent('خیلی خوب');
            return back();
        }

        $price=$course->price;
        $MerchantID = $this->MerchantID;
        $Description = 'توضیحات تراکنش تستی';
        $Email = auth()->user()->email;
        $CallbackURL = 'http://localhost::8000/course/payment/checker';
        $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);

        $result = $client->PaymentRequest(
            [
                'MerchantID' => $this->MerchantID,
                'Amount' => $price,
                'Description' => $Description,
                'Email' => $Email,
                'CallbackURL' => $CallbackURL,
            ]
        );

        if ($result->Status == 100) {

            auth()->user()->payments()->create([
                'course_id'=>$course->id,
                'resnumber'=>$result->Authority,
                'price'=>$price
            ]);
            return redirect('https://www.zarinpal.com/pg/StartPay/'.$result->Authority);
        } else {
            echo'ERR: '.$result->Status;
        }    }

    public function checker()
    {
        $Authority = $_GET['Authority'];
        $payment=Payment::whereResnumber($Authority)->firstOrFail();
        $course=Course::findOrFail($payment->course_id);
        if (\request(['Status']) == 'OK') {

            $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
            $result = $client->PaymentVerification(
                [
                    'MerchantID' => $this->MerchantID,
                    'Authority' => $Authority,
                    'Amount' => $payment->price,
                ]
            );
            if ($result->Status == 100) {
                if ($this->AddUserLearning($payment,$course)){
                    alert()->success('عملیات مورد نظر با موفقیت انجام شد','با تشکر');
                    return redirect($course->path());
                }
                echo 'Transaction success. RefID:'.$result->RefID;
            } else {
                echo 'Transaction failed. Status:'.$result->Status;
            }
        } else {
            echo 'Transaction canceled by user';
        }
    }

    public function AddUserLearning($payment,$course)
    {
        $payment->update(['approved'=>1]);
        Learning::create([
            'user_id'=>auth()->user()->id,
            'course_id'=>$course->id,
        ]);
        return true;
    }
}
