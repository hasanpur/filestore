<?php

namespace App\Http\Controllers;

use App\Article;
use App\Comment;
use App\Course;
use App\Jobs\SendMail;
use App\User;
use Artesaos\SEOTools\Facades\SEOMeta;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Queue\Jobs\Job;

class HomeController extends Controller
{
    public function index()
    {
//        $emailJob=(new SendMail(User::find(30),'hasanpour'));
//       ->delay(Carbon::now()->addSeconds(10))
//        ->onQueue("send:mail");
//        dispatch($emailJob);
        $lang = app()->getLocale();
        SEOMeta::setTitle(__('messages.title'));
        SEOMeta::setDescription('messages.description');

        if (cache()->has("articles.$lang")) {
            $articles = cache("articles.$lang");
        } else {
            $articles = Article::whereLang($lang)->latest()->take(8)->get();
            cache(["articles.$lang" => $articles], Carbon::now()->addMinutes(10));
        }
        if (cache()->has("courses.$lang")) {
            $courses = cache('courses');
        } else {
            $courses = Course::latest()->take(4)->get();
            cache(["articles.$lang" => $articles], Carbon::now()->addMinutes(10));
        }
        return view('Home.index', compact(['articles', 'courses']));
    }

    public function search()
    {
        $keywords=\request('search');
        if($keywords == null){
            return back();
        }
        if($keywords == 0){
            return "مقاله موردنظر یافت نشد.";
        }
        $article=Article::search($keywords)->latest()->get();
        return $article;

    }

    public function comment(Request $request)
    {
        $this->validate($request, [
            'comment' => 'required|min:5'
        ]);
        Comment::create(array_merge([
            'user_id' => auth()->user()->id,
        ], $request->all()));
//        auth()->user()->comments()->create($request->all());
        return back();

    }
}
