<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{

    protected $fillable = [
        'resnumber',
        'price',
        'course_id',
        'payment'
    ];
    public function scopeSpanningPayment($query, $month, $payment)
    {
        return $query->selectRaw('monthname(created_at) month , count(*) published')
            ->where('created_at', '>', Carbon::now()->subMonth($month))
            ->wherePayment($payment)
            ->groupBy('month')
            ->latest();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function course()
    {
        return $this->belongsTo(Course::class);
    }
}
