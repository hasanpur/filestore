<?php

namespace App\Providers;

use App\Comment;
use App\Payment;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        // recaptcha
        Validator::extend('recaptcha', function ($attribute, $value, $parameters, $validator) {
            // Post
            $client = new Client();
            $response = $client->request('POST', 'https://www.google.com/recaptcha/api/siteverify', [
                'form_params' => [
                    'secret' => config('services.recaptcha.secret'),
                    'response' => $value,
                    'remoteip' => request()->ip()
                ]
            ]);
            $response = json_decode($response->getBody());
            return $response->success;
        });
        // share view composer
        view()->composer('Admin.section.header', function ($view) {
            // comment
            $commentUnapprovedCount = Comment::whereApproved(0)->count();
            $commentApprovedCount = Comment::whereApproved(1)->count();
            // payment
            $paymentUnsuccessCount = Payment::wherePayment(0)->count();
            $paymentSuccessCount = Payment::wherePayment(1)->count();

            return $view->with([
                'commentUnapprovedCount' => $commentUnapprovedCount,
                'commentApprovedCount' => $commentApprovedCount,
                'paymentUnsuccessCount' => $paymentUnsuccessCount,
                'paymentSuccessCount' => $paymentSuccessCount,
            ]);
        });

    }
}
