<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use Sluggable;
    protected $fillable = [
        'user_id',
        'type',
        'title',
        'slug',
        'description',
        'body',
        'price',
        'images',
        'tags',
        'time',
        'viewCount',
        'commentCount',
    ];

    protected $guarded = [];

    protected $casts = [
        'images' => 'array'
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function setBodyAttribute($value)
    {
        $this->attributes['description'] = str_limit(preg_replace('/<[^>]*>/', '', $value), 200);
        $this->attributes['body'] = $value;
    }

    public function episodes()
    {
        return $this->hasMany(Episode::class);
    }

    public function path()
    {
        return "/courses/$this->slug";
    }

    /**
     * Get all of the post's comments.
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function scopeFilter($query)
    {
        $category = request('category');
        if (isset($category) && trim($category) != '' && $category=='all'){
            $query->whereHas('categories',function ($query) use ($category){
                $query->whereId($category);
            });
        }
        if (request('order')=='1'){
            $query->oldest();
        }else{
            $query->latest();
        }
        return $query;
    }
}

