<!DOCTYPE html>
<html>
<head>
    <title>Welcome Email</title>
</head>
<body>
<h2>{{$user['name']}}به سایت ما خوش آمدید</h2>
<br/>
برای ورود به سایت لازم است اکانت خود را فعالسازی نمایید .لطفا روی دکمه فعالسازی کلیک کنید
<br/>
<a href="{{url('user/verify', $user->verifyUser->token)}}">لینک فعالسازی</a>
</body>
</html>
