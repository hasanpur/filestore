<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'level' => 'user',
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});
$factory->define(App\Payment::class, function (Faker $faker) {

    return [
        'user_id' => function(){
        return User::all()->random();
        },
        'resnumber' => rand(10000000,50000000),
        'price' => rand(1000 , 50000),
        'payment' => rand(0,1),
        'created_at' => $faker->dateTimeBetween('-5 months' , 'now')
    ];
});
